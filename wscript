#! /usr/bin/env python
# vim: set fileencoding=utf-8 ts=4 sw=4 et

def configure(conf):
    f = conf.env.append_value
    f('INCLUDES',  [ conf.path.find_dir('src').abspath() ])
    f('DEFINES',   [ 'BOOST_USER_CONFIG=<ecpp/config/boost.hpp>' ])

def build(bld):
    pass 
    #bld.ecpp_build(
    #    target   = 'ecpp-boost',
    #    source   = bld.path.find_dir('src/').ant_glob('**/*.cpp'),
    #    features = 'cxx cstlib',
    #)