set(source_files
)

set(include_dirs
  ${CMAKE_CURRENT_SOURCE_DIR}/src
)

idf_component_register(SRCS "${source_files}"
                       INCLUDE_DIRS "${include_dirs}")