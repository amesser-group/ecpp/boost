######################
ECPP Boost Integration
######################

This module integrates a subset of the [boost]_ c++ libraries into the 
ecpp platform. Currently only the headers are made available in include path
No cpp source files are built.

Copyright & License
===================

See file LICENSE_1_0.txt for license information. 


References
=========

.. [boost] https://boostorg.jfrog.io/artifactory/main/release/1.77.0/source/boost_1_77_0.tar.bz2

